#!/usr/bin/python3
'''
Created on 09.01.2014

@author: jje
'''

from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui

from PyQt5.QtCore import QTimer
import datetime
import itertools
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import numpy
import serial, io
import sys
import glob
import os
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import configparser
import re

import serial_plotter_ui


class Data():

   def __init__(self, length, values):
      self.start_time = datetime.datetime.now()
      self.data = numpy.empty((len(values), int(length)))
      for i, j in zip(self.data, values):
         i.fill(j)
      self.time = numpy.zeros(int(length))

   def append(self, data):
      """
      Data expected to be seperated by \n
      """
      for i, d in enumerate(data):
         self.data[i] = numpy.roll(self.data[i], -1)
         self.data[i][-1] = d
      diff = datetime.datetime.now() - self.start_time
      self.time = numpy.roll(self.time, -1)
      self.time[-1] = diff.total_seconds()

   def __str__(self):
      out = ""
      for i, t in enumerate(self.time):
         out += "time {}: {}", t, ", ".join([str(value) for data in self.data for value in data[i]])
      return out



class Plot(QWidget):

   def __init__(self, parent=None):
      super(Plot, self).__init__(parent)
      self.figure = plt.Figure()
      self.canvas = FigureCanvas(self.figure)
      self.navi = NavigationToolbar(self.canvas, self)
      self.vbl = QVBoxLayout()
      self.vbl.addWidget(self.canvas)
      self.vbl.addWidget(self.navi)
      self.setLayout(self.vbl)
      self.plot = self.figure.add_subplot(111)

   def clear(self):
      plt.gcf().clear()
      # self.plot=self.figure.add_subplot(111)

   def show(self, times, data, xaxis_name, length, yaxis_name, y_width_neg, y_width_pos, autofocus_active,
            plot_meanval):
      self.plot.cla()
      color=['r', 'b', 'g', 'c', 'm', 'k']

      from cycler import cycler
      self.plot.set_prop_cycle(cycler('color',color))

      colors = cm.rainbow(numpy.linspace(0, 10, len(data)))
      for i, d, c in zip(itertools.count(), data, colors):
         self.plot.ticklabel_format(useOffset=False)
         self.plot.set_xlabel(xaxis_name)
         self.plot.set_ylabel(yaxis_name)
         self.plot.plot(times, d, marker='x', color=c, label=str(i))

         if (plot_meanval == True):
            self.plot.axhline(numpy.mean(d), linestyle='--', color=c)

         if (autofocus_active == False):
            self.plot.set_xlim(0, length)
            self.plot.set_ylim(y_width_neg, y_width_pos)

      self.canvas.draw()


class Config():

   def __init__(self):

      # Properties setup
      self.MAIN_CFG_SEC = 'MAIN_CONFIG'
      self.config_file = 'SerialPlotter.cfg'
      self.cfg_mean_val = 'cfg_mean_val'
      self.cfg_smpl_length = 'cfg_smpl_length'
      self.cfg_auto_focus = 'cfg_auto_focus'
      self.cfg_y_axis_max = 'cfg_y_axis_max'
      self.cfg_y_axis_min = 'cfg_y_axis_min'
      self.cfg_y_axis_unit = 'cfg_y_axis_unit'
      self.cfg_baudrate = 'cfg_baudrate'
      self.cfg_port = 'cfg_port'
      self.cfg_cycle_time = 'cfg_cycle_time'

      self.cfg_mean_val_default = None
      self.cfg_smpl_length_default = None
      self.cfg_auto_focus_default = None
      self.cfg_y_axis_max_default = None
      self.cfg_y_axis_min_default = None
      self.cfg_y_axis_unit_default = None
      self.cfg_baudrate_default = None
      self.cfg_port_default = None
      self.cfg_cycle_time_default = None

   def write_cfg(self, plotter):
      """
      Write Config file
      """

      # remove old config file
      try:
         os.remove(self.config_file)
      except(FileNotFoundError):
         print("No config file found, proceed.")

      config = configparser.RawConfigParser()

      config.add_section(self.MAIN_CFG_SEC)
      config.set(self.MAIN_CFG_SEC, self.cfg_mean_val, plotter.ui.checkBoxMeanvalue.isChecked())
      config.set(self.MAIN_CFG_SEC, self.cfg_smpl_length, int(plotter.ui.objSpinBoxLength.value()))
      config.set(self.MAIN_CFG_SEC, self.cfg_auto_focus, plotter.ui.checkBoxAutofocus.isChecked())
      config.set(self.MAIN_CFG_SEC, self.cfg_y_axis_max, int(plotter.ui.objSpinBoxYMax.value()))
      config.set(self.MAIN_CFG_SEC, self.cfg_y_axis_min, int(plotter.ui.objSpinBoxYMin.value()))
      config.set(self.MAIN_CFG_SEC, self.cfg_y_axis_unit, plotter.ui.objComboBoxYUnit.currentIndex())
      config.set(self.MAIN_CFG_SEC, self.cfg_baudrate, plotter.ui.objComboBoxBaudrate.currentIndex())
      config.set(self.MAIN_CFG_SEC, self.cfg_cycle_time, int(plotter.ui.objSpinBoxCycleTime.value()))
      # config.set(self.MAIN_CFG_SEC,self.cfg_port,plotter.ui.objComboBoxPort.currentText())           # Don't setup port as we don't know if port is available anyway. May be choosen manually

      # Writing our configuration file to 'example.cfg'
      with open(self.config_file, 'at', encoding='utf8') as configfile:
         config.write(configfile)

   def read_default(self, plotter):
      """
      Read default values in order to be able to setup defaults if needed.
      """
      self.cfg_mean_val_default = plotter.ui.checkBoxMeanvalue.isChecked()
      self.cfg_smpl_length_default = int(plotter.ui.objSpinBoxLength.value())
      self.cfg_auto_focus_default = plotter.ui.checkBoxAutofocus.isChecked()
      self.cfg_y_axis_max_default = int(plotter.ui.objSpinBoxYMax.value())
      self.cfg_y_axis_min_default = int(plotter.ui.objSpinBoxYMin.value())
      self.cfg_y_axis_unit_default = plotter.ui.objComboBoxYUnit.currentIndex()
      self.cfg_baudrate_default = plotter.ui.objComboBoxBaudrate.currentIndex()
      self.cfg_cycle_time_default = plotter.ui.objSpinBoxCycleTime.value()
      # self.cfg_port_default        = None                                                            # Don't setup port as we don't know if port is available anyway. May be choosen manually

   def setup_default(self, plotter):
      """
      Setup defaults if anything went wrong
      """
      # Get checkbox mean val.
      try:
         if 'True' == self.cfg_mean_val_default:
            plotter.ui.checkBoxMeanvalue.setChecked(True)
            plotter.show_meanval = True
         elif 'False' == self.cfg_mean_val_default:
            plotter.ui.checkBoxMeanvalue.setChecked(False)
            plotter.show_meanval = False
         else:
            Exception(KeyError)
      except KeyError:
         print("Loading default settings failed. End program.")
         return 3

      # Get length field content
      try:
         plotter.length = self.cfg_smpl_length_default
         plotter.ui.objSpinBoxLength.setValue(int(plotter.length))
      except KeyError:
         print("Loading default settings failed. End program.")
         return 3

         # Get checkbox autofocus
      try:
         if 'True' == self.cfg_auto_focus_default:
            plotter.ui.checkBoxAutofocus.setChecked(True)
         elif 'False' == self.cfg_auto_focus_default:
            plotter.ui.checkBoxAutofocus.setChecked(False)
         else:
            Exception(KeyError)
      except KeyError:
         print("Loading default settings failed. End program.")
         return 3

      # Get y axis max field content
      try:
         plotter.ui.objSpinBoxYMax.setValue(int(self.cfg_y_axis_max_default))
      except KeyError:
         print("Loading default settings failed. End program.")
         return 3

      # Get y axis min field content
      try:
         plotter.ui.objSpinBoxYMin.setValue(int(self.cfg_y_axis_min_default))
      except KeyError:
         print("Loading default settings failed. End program.")
         return 3

         # Set y axis unit
      try:
         plotter.ui.objComboBoxYUnit.setCurrentIndex(int(self.cfg_y_axis_unit_default))
      except KeyError:
         print("Loading default settings failed. End program.")
         return 3

         # Set choosen baudrate
      try:
         plotter.ui.objComboBoxBaudrate.setCurrentIndex(int(self.cfg_baudrate_default))
      except KeyError:
         print("Loading default settings failed. End program.")
         return 3

         # Get cycle time field content
      try:
         plotter.ui.objSpinBoxCycleTime.setValue(int(self.cfg_cycle_time_default))
      except KeyError:
         print("Loading default settings failed. End program.")
         return 3

   def read_cfg(self, plotter):
      """
      Read config file
      """

      self.read_default(plotter)  # Read default values in order to be able to setup defaults if needed.

      config = configparser.ConfigParser()
      x = config.read(self.config_file)
      if not x:
         print("Config file not found. Loading default settings.")
         self.setup_default(plotter)
         return 1

      # Get checkbox mean val.
      try:
         if 'True' == config[self.MAIN_CFG_SEC][self.cfg_mean_val]:
            plotter.ui.checkBoxMeanvalue.setChecked(True)
            plotter.show_meanval = True
         elif 'False' == config[self.MAIN_CFG_SEC][self.cfg_mean_val]:
            plotter.ui.checkBoxMeanvalue.setChecked(False)
            plotter.show_meanval = False
         else:
            Exception(KeyError)
      except KeyError:
         print("Reading config file failed. Loading default settings.")
         self.setup_default(plotter)
         return 2

      # Get length field content
      try:
         plotter.length = config[self.MAIN_CFG_SEC][self.cfg_smpl_length]
         plotter.ui.objSpinBoxLength.setValue(int(plotter.length))
      except KeyError:
         print("Reading config file failed. Loading default settings.")
         self.setup_default(plotter)
         return 2

         # Get checkbox autofocus
      try:
         if 'True' == config[self.MAIN_CFG_SEC][self.cfg_auto_focus]:
            plotter.ui.checkBoxAutofocus.setChecked(True)
         elif 'False' == config[self.MAIN_CFG_SEC][self.cfg_auto_focus]:
            plotter.ui.checkBoxAutofocus.setChecked(False)
         else:
            Exception(KeyError)
      except KeyError:
         print("Reading config file failed. Loading default settings.")
         self.setup_default(plotter)
         return 2

      # Get y axis max field content
      try:
         plotter.ui.objSpinBoxYMax.setValue(int(config[self.MAIN_CFG_SEC][self.cfg_y_axis_max]))
      except KeyError:
         print("Reading config file failed. Loading default settings.")
         self.setup_default(plotter)
         return 2

         # Get y axis min field content
      try:
         plotter.ui.objSpinBoxYMin.setValue(int(config[self.MAIN_CFG_SEC][self.cfg_y_axis_min]))
      except KeyError:
         print("Reading config file failed. Loading default settings.")
         self.setup_default(plotter)
         return 2

         # Set y axis unit
      try:
         plotter.ui.objComboBoxYUnit.setCurrentIndex(int(config[self.MAIN_CFG_SEC][self.cfg_y_axis_unit]))
      except KeyError:
         print("Reading config file failed. Loading default settings.")
         self.setup_default(plotter)
         return 2

         # Set choosen baudrate
      try:
         plotter.ui.objComboBoxBaudrate.setCurrentIndex(int(config[self.MAIN_CFG_SEC][self.cfg_baudrate]))
      except KeyError:
         print("Reading config file failed. Loading default settings.")
         self.setup_default(plotter)
         return 2

         # Get cycle time field content
      try:
         plotter.ui.objSpinBoxCycleTime.setValue(int(config[self.MAIN_CFG_SEC][self.cfg_cycle_time]))
      except KeyError:
         print("Reading config file failed. Loading default settings.")
         self.setup_default(plotter)
         return 2


class Plotter():

   def __init__(self):

      # Main state attributes
      self.running = False
      self.connected = False

      # Common attributes
      self.length = 10  # Length of plot window in points
      self.cycle_time = 100  # Cycle time - check for incoming data on serial interface(mSec.)
      self.multiply_factor = 1000 / self.cycle_time  # Calculate multiply factor in order to be able to adapt calculations of axis setup
      self.record_time = -1  # Duration recording process
      self.channel = 1  # Channel which is to be observed

      # Flags
      self.autofocus_active = False  # autofocus active/inactive
      self.show_meanval = False  # Show meanvalue in seperate line
      self.logg_ON = False  # Logging into CSV File ON[True]/OFF[False]
      self.recording_ON = False  # recording process ON

      # Prepare main UI-Class
      self.window = QMainWindow()
      self.ui = serial_plotter_ui.Ui_MainWindow()
      self.ui.setupUi(self.window)

      # init expected elements
      self.init_combo_boxes()
      self.init_event_handlers()

      # Prepare elements
      if ("" != self.ui.objComboBoxPort.currentText()):
         self.ui.bttConnect.setEnabled(True)
      else:
         self.ui.bttConnect.setEnabled(False)

      self.ui.objSpinBoxLength.setEnabled(True)
      self.ui.checkBoxMeanvalue.setEnabled(False)

      # Setup plot attributes
      self.window.show()  # Show main window
      self.XAxisName = "Seconds"  # XAxis name
      self.YAxisName = None  # YAxis name
      self.y_width_neg = 0
      self.y_width_pos = 10
      self.autofocus = False  # Activate autofocus
      self.plot_med = False  # Plot mean value as seperate line
      self.length = self.ui.objSpinBoxLength.value()  # Setup Spin Box XPoints

      # Common attributes to serial interface
      self.serial_baud = None
      self.serial_port = None
      self.serial = False  # Serial interface choosen
      self.app_tick_timer = None
      self.record_timer = None
      self.record_timer_start_time = 0
      self.sio = None

      # Data expected
      self.values = []
      self.data = None
      self.max_val = 0
      self.min_val = 0
      self.unknown_char = 0
      self.reset_count = 0

      # Configuration
      self.config = Config()
      self.config.read_cfg(self)  # Try to read config file if available

   def search_serial_ports(self):
      """
      Lists serial port names

      :raises EnvironmentError:
          On unsupported or unknown platforms
      :returns:
          A list of the serial ports available on the system
      """
      if sys.platform.startswith('win'):
         temp_list = ['COM%s' % (i + 1) for i in range(256)]
      else:
         temp_list = glob.glob('/dev/tty[A-Za-z]*')

      result = []
      for a_port in temp_list:

         try:
            s = serial.Serial(a_port)
            s.close()
            result.append(a_port)
         except serial.SerialException:
            pass

      return result

   def check_rec_time(self):
      """
      Check record time setup
      """
      val = self.ui.objSpinBoxRecTime.value()
      if self.record_timer and self.record_timer.isActive():
         self.record_timer.stop()
         self.record_timer_start_time = 0

      if val == -1:
         # Turned off(in case <9 sec.
         self.recording_ON = False
         self.record_time = -1
         self.ui.objLcdNumRecTimeRem.display(0)
      else:
         # Turned on
         self.recording_ON = True
         self.record_time = val * 1000
         self.record_timer = QTimer()
         self.record_timer.timeout.connect(self.stop_run)

         if self.running == True:
            self.record_timer_start_time = datetime.datetime.now()
            self.record_timer.start(self.record_time)
            self.resetAll()

   def check_run_state(self):
      """
      Check run state
      """
      if (self.running):
         self.ui.bttStartStop.setText("Start")
         self.app_tick_timer.stop()
         self.running = False
         self.ui.objSpinBoxCycleTime.setEnabled(True)

         if self.record_timer and self.record_timer.isActive():
            self.record_timer.stop()
      else:
         self.ui.bttStartStop.setText("Stop")
         self.ui.objSpinBoxCycleTime.setEnabled(False)
         self.serial.flushInput()
         self.app_tick_timer.start(self.cycle_time)
         self.running = True
         self.check_rec_time()

   def stop_run(self):
      """
      Record time is up
      """
      self.check_run_state()

   def init_event_handlers(self):
      """
      Init event handlers
      """
      # Set click action
      self.ui.bttQuit.clicked.connect(self.clickBttQuit)
      self.ui.bttStartStop.clicked.connect(self.clickBttRun)
      self.ui.bttRefreshPortlist.clicked.connect(self.clickBttRefresh)
      self.ui.bttConnect.clicked.connect(self.clickBttConnect)
      self.ui.bttReset.clicked.connect(self.clickBttReset)
      self.ui.bttStoreSetup.clicked.connect(self.clickBttStoreSetup)
      self.ui.checkBoxAutofocus.stateChanged.connect(self.checkBoxAutofocusStateChngd)
      self.ui.checkBoxMeanvalue.stateChanged.connect(self.checkBoxMeanvalueChngd)
      self.ui.objSpinBoxLength.valueChanged.connect(self.spinBoxLengthValueChanged)
      self.ui.objSpinBoxYMin.valueChanged.connect(self.spinBoxYMinChanged)
      self.ui.objSpinBoxYMax.valueChanged.connect(self.spinBoxYMaxChanged)
      self.ui.objSpinBoxCycleTime.valueChanged.connect(self.spinBoxCycleTimeChanged)
      self.ui.objSpinBoxRecTime.valueChanged.connect(self.spinBoxRecTimeValueChanged)
      self.ui.checkBoxLogg.stateChanged.connect(self.checkBoxLoggStateChngd)
      self.ui.objSpinBoxChannel.valueChanged.connect(self.spinBoxChannelValueChanged)

   def spinBoxChannelValueChanged(self):
      """
      Selected Channel changed(We do parse incoming string correspondigly)
      """
      self.channel = self.ui.objSpinBoxChannel.value()

   def spinBoxRecTimeValueChanged(self):
      """
      Record time changed
      """
      self.check_rec_time()

   def spinBoxCycleTimeChanged(self):
      """
      Cycle time changed
      """
      self.cycle_time = self.ui.objSpinBoxCycleTime.value()
      self.multiply_factor = 1000 / self.cycle_time  # Calculate multiply factor in order to be able to adapt calculations of axis setup

   def spinBoxLengthValueChanged(self):
      """
      Length for autofocus setup changed
      """
      self.length = self.ui.objSpinBoxLength.value()
      self.reinit_base()

   def spinBoxYMinChanged(self):
      """
      Length for autofocus setup changed
      """
      self.y_width_neg = self.ui.objSpinBoxYMin.value()

   def spinBoxYMaxChanged(self):
      """
      Length for autofocus setup changed
      """
      self.y_width_pos = self.ui.objSpinBoxYMax.value()

   def init_combo_boxes(self):

      # Setup list Baudrate
      listComboBoxBaudrate = [
         "9600",
         "19200",
         "38400",
         "57600",
         "115200",
         "230400",
         "921600"
      ]

      self.ui.objComboBoxBaudrate.clear()
      self.ui.objComboBoxBaudrate.addItems(listComboBoxBaudrate)
      self.ui.objComboBoxBaudrate.setCurrentIndex(0)

      # Setup list Unit Y-Axis
      listComboBoxYUnit = [
         "mCelsius",
         "Celsius",
         "mOhm",
         "Ohm",
         "mV",
         "V",
      ]

      self.ui.objComboBoxYUnit.clear()
      self.ui.objComboBoxYUnit.addItems(listComboBoxYUnit)
      self.ui.objComboBoxYUnit.setCurrentIndex(0)

      # scan for available Ports
      self.ui.objComboBoxPort.clear()
      self.ui.objComboBoxPort.addItems(self.search_serial_ports())
      self.ui.objComboBoxPort.setCurrentIndex(0)

   def clickBttStoreSetup(self):
      """
      Store Setup in order to be able to
      read last config if possible.
      """
      self.config.write_cfg(self)

   def resetAll(self):
      """
      Reset func.
      """
      if (self.data != None):
         self.reinit_base()

      # remove old log file
      try:
         if self.file_handler != None:
            self.file_handler.close()
            os.remove(self.outfile)
            self.file_handler = open(self.outfile, 'a+')  # File handler
      except(FileNotFoundError):
         print("No logging file found, proceed.")

   def clickBttReset(self):
      """
      Reset Meas.
      """
      self.resetAll()
      self.check_rec_time()

   def clickBttQuit(self):
      """
      Quit application
      """
      if (self.running):
         self.ui.bttStartStop.setText("Start")
         self.app_tick_timer.stop()
         self.running = False

      if (self.connected):
         self.ui.bttConnect.setText("Connect")
         self.connected = False

         # Enable all the elements, which may not be modified
         self.ui.objComboBoxPort.setEnabled(True)
         self.ui.objComboBoxBaudrate.setEnabled(True)
         self.ui.objComboBoxYUnit.setEnabled(True)
         self.app_tick_timer.stop()
         self.serial.close()
         if self.file_handler != None:
            self.file_handler.close()

         # disable&reset elements needed for run
         self.ui.bttStartStop.setEnabled(False)
         self.ui.bttReset.setEnabled(False)
         self.ui.bttStartStop.setText("Start")
         self.running = False

      app.quit()

   def clickBttConnect(self):
      """
      Connect to serial interface.
      """

      if (self.connected):
         self.ui.bttConnect.setText("Connect")
         self.connected = False

         # Enable all the elements, which may not be modified
         self.ui.objComboBoxPort.setEnabled(True)
         self.ui.objComboBoxBaudrate.setEnabled(True)
         self.ui.objComboBoxYUnit.setEnabled(True)
         self.app_tick_timer.stop()
         self.serial.close()
         if self.file_handler != None:
            self.file_handler.close()
            self.file_handler = None

            # disable&reset elements needed for run
         self.ui.bttStartStop.setEnabled(False)
         self.ui.bttReset.setEnabled(False)
         self.ui.bttStartStop.setText("Start")
         self.running = False

         self.ui.checkBoxLogg.setEnabled(True)


      else:
         self.ui.bttConnect.setText("Disconnect")
         self.connected = True

         # Setup serial
         self.serial_port = self.ui.objComboBoxPort.currentText()
         self.serial_baud = self.ui.objComboBoxBaudrate.currentText()
         self.serial = serial.Serial(port=self.serial_port,
                                     baudrate=self.serial_baud,
                                     bytesize=serial.EIGHTBITS,
                                     parity=serial.PARITY_NONE,
                                     stopbits=serial.STOPBITS_ONE
                                     )
         self.sio = io.TextIOWrapper(io.BufferedRWPair(self.serial, self.serial, 1), encoding='ascii', newline='\n')

         # Setup window and plot properties
         self.YAxisName = self.ui.objComboBoxYUnit.currentText()
         self.length = self.ui.objSpinBoxLength.value()
         self.y_width_neg = self.ui.objSpinBoxYMin.value()
         self.y_width_pos = self.ui.objSpinBoxYMax.value()
         self.reinit_base()

         # disable all the elements, which may not be modified
         self.ui.objComboBoxPort.setEnabled(False)
         self.ui.objComboBoxBaudrate.setEnabled(False)
         self.ui.objComboBoxYUnit.setEnabled(False)

         # enable required elements
         self.ui.bttStartStop.setEnabled(True)
         self.ui.bttReset.setEnabled(True)

         # Setup timer for - update routine on timer event(as fileio may not be used on windows)
         self.serial.flushInput()
         self.app_tick_timer = QTimer()
         self.app_tick_timer.timeout.connect(self.update)

         self.ui.checkBoxLogg.setEnabled(False)

   def clickBttRun(self):
      self.check_run_state()

   def clickBttRefresh(self):
      """
      Refresh serial port list
      """
      # scan for available Ports
      self.ui.objComboBoxPort.clear()
      self.ui.objComboBoxPort.addItems(self.search_serial_ports())
      self.ui.objComboBoxPort.setCurrentIndex(0)

      # Check if any devices found
      if ("" != self.ui.objComboBoxPort.currentText()):
         self.ui.bttConnect.setEnabled(True)
      else:
         self.ui.bttConnect.setEnabled(False)

   def checkBoxMeanvalueChngd(self):
      """
      Track state checkBoxAutofocus
      """
      if self.ui.checkBoxMeanvalue.isChecked():
         self.show_meanval = True
      else:
         self.show_meanval = False

   def checkBoxAutofocusStateChngd(self):
      """
      Track state checkBoxAutofocus
      """
      if self.ui.checkBoxAutofocus.isChecked():
         self.autofocus_active = True
         self.ui.objSpinBoxYMin.setEnabled(False)
         self.ui.objSpinBoxYMax.setEnabled(False)
         self.ui.checkBoxMeanvalue.setEnabled(True)
         self.reinit_base()
      else:
         self.autofocus_active = False
         self.ui.objSpinBoxYMin.setEnabled(True)
         self.ui.objSpinBoxYMax.setEnabled(True)
         self.ui.checkBoxMeanvalue.setEnabled(False)
         self.reinit_base()

   def checkBoxLoggStateChngd(self):
      """
      Logging in csv activated
      """
      if self.ui.checkBoxLogg.isChecked():
         self.logg_ON = True
      else:
         self.logg_ON = False


class ReadPlotter(Plotter):

   def __init__(self):
      Plotter.__init__(self)

      # Attrubutes used for logging
      self.outfile = os.getcwd() + "/SerialLog.csv"  # Current working dir as path
      self.file_handler = None  # File handler
      self.logFileIndx = 1  # Log file name index
      self.fileSizeSupervisor = 20  # Timeout in mins, after which we're going to rename logfile and append index
      self.start_time = datetime.datetime.now()
      self.line = ""
      self.meas_val = []
      self.meas_val_max = 0
      self.meas_val_min = 10000000

   def writeLogFile(self, diffSec, datastring):
      #
      if self.file_handler == None:
         self.file_handler = open(self.outfile, 'a+')

      # Logg data(and remove \newline if given in string)
      self.file_handler.write(str(diffSec) + '\t' + str(datastring.rstrip()))
      self.file_handler.write('\n')

      # Rename file if needed in order to limit file size
      # as log may run for long time
      if diffSec / (60 * self.logFileIndx) > self.fileSizeSupervisor:
         self.file_handler.close()

         os.rename(self.outfile, self.outfile.replace(self.outfile, self.outfile + str(self.logFileIndx)))
         self.logFileIndx = self.logFileIndx + 1

         self.file_handler = open(self.outfile, 'wb')

   def serial_readline_nonblocking(self, numOfBytes):
      """
      Readline in nonblocking manner
      *Check for maximum length on newline
      *Check for valid chars
      *Check for RESET
      """
      for c in self.serial.read(numOfBytes):
         try:
            self.line = self.line + (chr(c))

            # Check length
            if len(self.line) > 100:
               raise ValueError("Received nonsense, drop data.")

            # received newline
            if chr(c) == "\n":

               # Check for reset char
               if re.search("[RESET]", self.line):
                  print("SOFT+" + str(self.line))
                  raise EnvironmentError("Received RESET string.")

                  # Meas value

               temp = self.line.split()
               temp2 = int(temp[int(self.channel) - 1])
               #temp3 = temp2 / 1000

               # plausibillity check - received numeber?
               self.meas_val.clear()
               # self.meas_val.append(temp[int(self.channel)-1])
               self.meas_val.append(str(temp2))
               comp_val = float(str(self.meas_val[0]))
               print("VAL:%s   REC:[%s] " % (str(self.meas_val), temp))

               # Get Max/Min val
               if comp_val > self.meas_val_max:
                  self.meas_val_max = comp_val
                  self.ui.objLcdNumPeakMAX.display(self.meas_val_max)
               if comp_val < self.meas_val_min:
                  self.meas_val_min = comp_val
                  self.ui.objLcdNumPeakMIN.display(self.meas_val_min)

               self.line = ""
         except ValueError:
            # received nonsense or not a number
            self.unknown_char = self.unknown_char + 1
            self.ui.objLcdNumUnknownChar.display(self.unknown_char)
            print("ValueError: " + str(self.line))
            self.line = ""
         except IndexError:
            # rcvd. empty line
            pass
         except EnvironmentError:
            self.reset_count = self.reset_count + 1
            self.ui.objLcdNumReset.display(self.reset_count)
            self.line = ""

      return True

   #  def serial_readline_nonblocking(self,numOfBytes):
   #      """
   #      Readline in nonblocking manner
   #      *Check for maximum length on newline
   #      *Check for valid chars
   #      *Check for RESET
   #      """
   #      for c in self.serial.read(numOfBytes):
   #          try:
   #              self.line = self.line + (chr(c))
   #
   #
   #              #Check length
   #              if len(self.line)>100:
   #                  raise ValueError("Received nonsense, drop data.")
   #
   #              #received newline
   #              if chr(c) == "\n":
   #
   #                  if len(self.line) > 2:
   #                      #Check for reset char
   #                      if re.search("[RESET]", self.line):
   #                          print("SOFT+"+str(self.line))
   #                          raise EnvironmentError("Received RESET string.")
   #
   #                      #Meas value
   #
   #                      temp=self.line.split('\n')[0]
   #                      print("Received:"+temp)
   #                      #temp=self.line
   #                      temp2 = int(temp)-273150
   #                      temp3 = temp2/1000
   #
   #                      #plausibillity check - received numeber?
   #                      self.meas_val.clear()
   #                      #self.meas_val.append(temp[int(self.channel)-1])
   #                      self.meas_val.append(str(temp3))
   #                      comp_val=float(str(self.meas_val[0]))
   #                      print("VAL:%s   REC:[%s] "%(str(self.meas_val),temp))
   #
   #
   #                      #Get Max/Min val
   #                      if comp_val > self.meas_val_max:
   #                          self.meas_val_max=comp_val
   #                          self.ui.objLcdNumPeakMAX.display(self.meas_val_max)
   #                      if comp_val < self.meas_val_min:
   #                          self.meas_val_min=comp_val
   #                          self.ui.objLcdNumPeakMIN.display(self.meas_val_min)
   #
   #                      self.line=""
   #                  else:
   #                      self.line=""
   #          except ValueError:
   #              #received nonsense or not a number
   #              self.unknown_char=self.unknown_char+1
   #              self.ui.objLcdNumUnknownChar.display(self.unknown_char)
   #              print("ValueError: "+str(self.line))
   #              self.line=""
   #          except EnvironmentError:
   #              self.reset_count=self.reset_count+1
   #              self.ui.objLcdNumReset.display(self.reset_count)
   #              self.line=""
   #
   #
   #
   #      return True

   def serial_read(self):
      if self.serial.inWaiting() > 0:
         if self.serial.isOpen():
            # line=self.serial.readline()    Not allowed; We need to use readline in nonblocking manner
            if self.serial_readline_nonblocking(self.serial.inWaiting()):
               diff = datetime.datetime.now() - self.start_time
               if self.logg_ON == True:
                  # print("Logg sec: "+str(diff.total_seconds()))
                  # print("Log VAL: "+str(line))
                  self.writeLogFile(diff.total_seconds(), str(self.meas_val))
               return self.meas_val
            else:
               return None
         else:
            return None
      else:
         return None

   def reinit_base(self):
      """
      Called on changes or reset, we need to setup data module accordingly
      """
      self.data = None
      self.meas_val_max = 0
      self.meas_val_min = 10000000
      self.unknown_char = 0
      self.reset_count = 0

      self.ui.objLcdNumUnknownChar.display(self.unknown_char)
      self.ui.objLcdNumReset.display(self.reset_count)

   def update(self):
      data = self.serial_read()

      if True == self.recording_ON:
         diff = datetime.datetime.now() - self.record_timer_start_time
         self.ui.objLcdNumRecTimeRem.display(diff.total_seconds())

      if self.data == None and data != None:
         # Depending on selected
         self.data = Data(self.length * self.multiply_factor,
                          [i for i in data])  # We need to multiply datapoints with delta on x axis

      # Read data on serial(We may start reading without getting any data via serial terminal.
      if data != None:
         self.data.append(data)
         self.ui.plot.show(self.data.time, self.data.data, self.XAxisName, self.length, self.YAxisName,
                           self.y_width_neg, self.y_width_pos, self.autofocus_active, self.show_meanval)


if __name__ == '__main__':
   app = QApplication(sys.argv)
   plot = ReadPlotter()
   sys.exit(app.exec_())
